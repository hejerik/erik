var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var path = require('path')

var names = []

app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', function(req, res) {
    res.send(listPage(names))
})

app.get('/font.ttf', function(req, res) {
    res.sendFile(path.join(__dirname + '/font.ttf'))
})

app.get('/adder', function(req, res) {
    res.sendFile(path.join(__dirname + '/add.html'))
})

app.post('/add', function(req, res) {
    names.push(req.body.name)
    res.sendFile(path.join(__dirname + '/add.html'))
})

app.listen(3000)

function listPage(names) {
    const nameList = names.map(function(name) { return listItem(name) }).join("")
    return `
	<html>
	<head>
	    <style>${styling()}</style>
	</head>
	<body>
	    <ul>
	    	${nameList}
	    </ul>
	</body>
	</html>
	`	
}

function listItem(name) {
    return `<li>${name}</li>`
}

function styling() {
    return `
	    li {
	    	font-size: 3em;
		text-align: center;
	    }
	    ul {
		list-style: none;
		margin:auto;
		width: 50%;
		padding: 0;
		}
	    }
	`
}

